//
//  ViewController.swift
//  DreamerNews
//
//  Created by Rayen on 11/10/16.
//  Copyright © 2016 GeeksDoByte. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase


class ViewController: UIViewController {
var ref: FIRDatabaseReference!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // [START create_database_reference]
        ref = FIRDatabase.database().reference()
        // [END create_database_reference]
   

        ref.child("sources").queryOrderedByKey().observeSingleEvent(of: .value, with:{(snap) in
            
            print(snap)
            
        })
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



}

