//
//  ViewController.swift
//  TestBML
//
//  Created by Rayen on 11/18/16.
//  Copyright © 2016 GeeksDoByte. All rights reserved.
//
//  There is no stable framework for Swift 3.0 I used swift 2.3 for the Reactive Frameworks, rest of project is written swift 3
// https://www.linkedin.com/in/rayenkamta
// https://itunes.apple.com/us/developer/rayen-kamta/id798935537
import Foundation
import ReactiveJSON
import ReactiveCocoa
import Result
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
   
        
        
        self.getComments()
        self.getPosts()
        self.getUsers()
        
    }
    
    
    func getPosts(){
        TYPICODEAPI.request(endpoint: "posts").startWithResult{ (result: Result<AnyObject, NetworkError>) in
            switch result {
            case .Success(let result):
                print("Results: \(result)")
                SavedPostsBox.sharedInstance.postData = result as! NSArray
                
            case .Failure(let error):
                print("Error: \(error)")
            }
            
        }
        
    }
    
    func getUsers(){
        TYPICODEAPI.request(endpoint: "users").startWithResult{ (result: Result<AnyObject, NetworkError>) in
            switch result {
            case .Success(let result):
                print("Results: \(result)")
                SavedPostsBox.sharedInstance.userData = result as! NSArray
                
            case .Failure(let error):
                print("Error: \(error)")
            }
            
        }
    }
    
    func getComments(){
        TYPICODEAPI.request(endpoint: "comments").startWithResult{ (result: Result<AnyObject, NetworkError>) in
            switch result {
            case .Success(let result):
                print("Results: \(result)")
                SavedPostsBox.sharedInstance.commentData = result as! NSArray
                
            case .Failure(let error):
                print("Error: \(error)")
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

