//
//  JSONClient.swift
//  TestBML
//
//  Created by Rayen on 11/18/16.
//  Copyright © 2016 GeeksDoByte. All rights reserved.
//

import Foundation

/// A JSON client for the TYPICODEAPI API
struct TYPICODEAPI: Singleton, ServiceHost {
    // 'Singleton'
    private(set) static var shared = Instance()
    typealias Instance = TYPICODEAPI
    
    // 'ServiceHost'
    static var scheme: String { return "http" }
    static var host: String { return "jsonplaceholder.typicode.com" }
    static var path: String? { return nil }
}


class SavedPostsBox{
    var postData:NSArray = []
    var userData:NSArray = []
    var commentData:NSArray = []
    
    
    
    
    class var sharedInstance : SavedPostsBox {
        struct Static {
            static let instance : SavedPostsBox = SavedPostsBox()
        }
        return Static.instance
    }
}
