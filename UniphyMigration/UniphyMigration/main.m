//
//  main.m
//  UniphyMigration
//
//  Created by Rayen on 11/4/16.
//  Copyright © 2016 GeeksDoByte. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
