//
//  ViewController.m
//  UniphyMigration
//
//  Created by Rayen on 11/4/16.
//  Copyright © 2016 GeeksDoByte. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * passStr = [defaults valueForKey:@"passWord"];
    NSString * userSTR = [defaults valueForKey:@"userName"];
    
    self.passlbl.text = passStr;
    self.userlbl.text = userSTR;
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
