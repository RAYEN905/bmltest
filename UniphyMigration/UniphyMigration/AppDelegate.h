//
//  AppDelegate.h
//  UniphyMigration
//
//  Created by Rayen on 11/4/16.
//  Copyright © 2016 GeeksDoByte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

