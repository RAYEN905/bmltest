//
//  EnterTextViewController.swift
//  SavageKermit
//
//  Created by Rayen on 11/17/16.
//  Copyright © 2016 GeeksDoByte. All rights reserved.
//

import UIKit
import GoogleMobileAds

class EnterTextViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet weak var kermitTxt: UITextField!
    @IBOutlet weak var savageTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.adUnitID = kBannerAdUnitID 
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        // Do any additional setup after loading the view.
        
         self.hideKeyboardWhenTappedAround()
        kermitTxt.delegate = self;
        savageTxt.delegate = self;
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */


    

    /*
  
    // MARK: - Navigation


    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func pressPreview(_ sender: UIButton) {
        SavedPostsBox.sharedInstance.meStr = kermitTxt.text! as String
        SavedPostsBox.sharedInstance.savageStr = savageTxt.text! as String
    }
    @IBOutlet weak var pressPreview: UIButton!
  

}
