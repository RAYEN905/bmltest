//
//  SavedPostsBox.swift
//  SavageKermit
//
//  Created by Rayen on 11/17/16.
//  Copyright © 2016 GeeksDoByte. All rights reserved.
//


import Foundation
class SavedPostsBox{
    var meStr: String = ""
    var savageStr: String = ""
    
    
    class var sharedInstance : SavedPostsBox {
        struct Static {
            static let instance : SavedPostsBox = SavedPostsBox()
        }
        return Static.instance
    }  
}
