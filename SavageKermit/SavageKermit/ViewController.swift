//
//  ViewController.swift
//  SavageKermit
//
//  Created by Rayen on 11/17/16.
//  Copyright © 2016 GeeksDoByte. All rights reserved.
//

import UIKit
import GoogleMobileAds

let kBannerAdUnitID = "ca-app-pub-2005397033893588/9417260187"


class ViewController: UIViewController  {
    
   
    
    @IBOutlet weak var bannerView: GADBannerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       bannerView.adUnitID = kBannerAdUnitID
       bannerView.rootViewController = self
       bannerView.load(GADRequest())
     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }
}


extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

