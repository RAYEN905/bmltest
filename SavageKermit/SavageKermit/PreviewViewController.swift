//
//  PreviewViewController.swift
//  SavageKermit
//
//  Created by Rayen on 11/17/16.
//  Copyright © 2016 GeeksDoByte. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController {
    @IBOutlet weak var meTxtLabel: UILabel!
    @IBOutlet weak var savageTxtLabel: UILabel!

    @IBOutlet weak var memeView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        meTxtLabel.text = "Me: " + SavedPostsBox.sharedInstance.meStr
        savageTxtLabel.text = "Savage Me: " + SavedPostsBox.sharedInstance.savageStr    
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func pressSave(_ sender: Any) {
        let view : UIView = self.memeView//Any view can be here!
        _ = view.getSnapshotImage()
        UIImageWriteToSavedPhotosAlbum(view.getSnapshotImage(), self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        
       
    }
    func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIView {
    func getSnapshotImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: false)
        let snapshotImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return snapshotImage
    }
}
