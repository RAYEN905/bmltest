//
//  CarWebViewController.swift
//  Offline Arcade
//
//  Created by Rayen on 11/2/16.
//  Copyright © 2016 GeeksDoByte. All rights reserved.
//

import UIKit

class CarWebViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
//        let htmlFile = Bundle.main.path(forResource: "index", ofType: "html")
//        let htmlString = try? String(contentsOfFile: htmlFile!, encoding: String.Encoding.utf8)
//        webView.loadHTMLString(htmlString!, baseURL: nil)
//        
        if let url = Bundle.main.url(forResource: "index", withExtension: "html") {
            webView.loadRequest(NSURLRequest(url: url) as URLRequest)
        }
        
//        
//        if let url = URL(string: "http://geeksdobyte.com/games/HTML5/index.html") {
//            do {
//                let contents = try String(contentsOf: url)
//                print(contents)
//            } catch {
//                // contents could not be loaded
//            }
//        } else {
//            // the URL was bad!
//        }
   }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
